import matplotlib.pyplot as plt

def graph(name):
    x = []
    ask = []
    bid = []
    with open(f"{name}.txt", "r") as f:
        lines = f.readlines()
        counter = 0
        for line in lines:
            if counter < 1:
                counter += 1
            else:
                arr = line.strip().split(",")
                ask.append(float(arr[0].strip()))
                bid.append(float(arr[1].strip()))
                x.append(float(arr[2].strip()))

    plt.clf()
    plt.plot(x, ask, label="ask")
    plt.plot(x, bid, label="bid")
    plt.title(name)
    plt.legend()
    plt.savefig(f"{name}.png")

graph("XETHZUSD_20201123")
graph("XXBTZUSD_20201123")
graph("BCHUSD_20201123")