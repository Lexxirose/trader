import os
import requests
import time
from datetime import datetime
import json

def ticker(pair):
        payload = {"pair": pair}
        r = requests.post("https://api.kraken.com/0/public/Ticker", data=payload)
        data = r.json()
        res = {
            "ask": float(data["result"][pair]["a"][0]),
            "bid": float(data["result"][pair]["b"][0])
        }

        return res

def track_market(pair):
        t = ticker(pair)
        now = datetime.now()
        hr = now.hour
        mi = now.minute
        sec = now.second
        tot_time = hr + (mi/60) + (sec/3600)
        directory = str(now.year) + str(now.month) + str(now.day)
        if not os.path.isdir(directory):
            os.mkdir(directory)
        filename = pair + ".txt"
        with open(f"{directory}/{filename}", "a") as f:
            s = f"{t['ask']}, {t['bid']}, {tot_time}"
            f.write("\n")
            f.write(s)


running = True

while running:
    track_market("XETHZUSD")
    track_market("XXBTZUSD")
    track_market("BCHUSD")
    time.sleep(10)