import requests
import json
from datetime import datetime

class Kraken:
    def __init__(self, funds):
        self.funds = funds
        self.bought = False
        self.amount = 0
        self.prev_funds = 0

    def get_tradable_assets(self, name):
        r = requests.get("https://api.kraken.com/0/public/AssetPairs")
        data = r.json()
        res = []
        for i in data["result"]:
            if i == name:
                res = {
                    "name": i,
                    "base": data["result"][i]["base"],
                    "quote": data["result"][i]["quote"],
                    "fees": data["result"][i]["fees"]
                }
        return res



    def ticker(self, pair):
        payload = {"pair": pair}
        r = requests.post("https://api.kraken.com/0/public/Ticker", data=payload)
        data = r.json()
        res = {
            "ask": float(data["result"][pair]["a"][0]),
            "bid": float(data["result"][pair]["b"][0])
        }

        return res

    def trade(self):
        t = self.ticker("XETHZUSD")
        gta = self.get_tradable_assets("XETHZUSD")
        if not self.bought:
            amount = self.funds/t["ask"] - (self.funds/t["ask"]*float(gta["fees"][0][1])/100)
            self.prev_funds = self.funds
            self.funds -= self.funds
            self.amount = amount
            profit = self.amount*t["bid"]*(1-(float(gta["fees"][0][1])/100))
            self.bought = True
            print(f"Bought. funds: {self.funds}")
            return profit/self.prev_funds

        else:
            profit = self.amount*t["bid"]*(1-(float(gta["fees"][0][1])/100))
            if (profit/self.prev_funds > 1):
                amount = self.amount*t["bid"]
                self.funds += amount
                self.index = 0
                self.bought = False
                print(f"Percentage: {profit/self.prev_funds}")
                print(f"Sold. funds: {self.funds}")
            else:
                print(f"Percentage: {profit/self.prev_funds}")
                print("Did nothing")
        
            return profit/self.prev_funds

    def track_market(self, pair):
        t = self.ticker(pair)
        now = datetime.now()
        hr = now.hour
        mi = now.minute
        sec = now.second
        tot_time = hr + (mi/60) + (sec/3600)
        filename = pair + "_" + str(now.year) + str(now.month) + str(now.day) + ".txt"
        with open(filename, "a") as f:
            s = f"{t['ask']}, {t['bid']}, {tot_time}"
            f.write("\n")
            f.write(s)


