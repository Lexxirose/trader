from Kraken import *
import requests
import time
import matplotlib.pyplot as plt

# "XXBTZUSD"


eth = Kraken(10000)
btc = Kraken(10000)
bch = Kraken(10000)

running = True



while running:
    eth.trade("XETHZUSD")
    btc.trade("XXBTZUSD")
    bch.trade("BCHUSD")
    time.sleep(10)